from django.contrib import admin
from django.contrib.auth import get_user_model
from baquara.users.admin import MyUserAdmin
from baquara_seed.models import baquara_seedProfile


User = get_user_model()
# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class baquara_seedProfileInline(admin.StackedInline):
    model = baquara_seedProfile
    can_delete = False
    verbose_name_plural = 'baquara_seed'

# Define a new User admin
class SindesepUserAdmin(MyUserAdmin):
    inlines = (baquara_seedProfileInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, SindesepUserAdmin)