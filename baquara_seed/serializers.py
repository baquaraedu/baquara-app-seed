from rest_framework import serializers
from rest_framework import status
from rest_framework.response import Response

from discussion.serializers import BaseTopicSerializer, BaseCommentSerializer, TopicLikeSerializer, CommentLikeSerializer
from accounts.serializers import GroupSerializer, GroupAdminSerializer
from core.models import Class, Course, CertificateTemplate
from core.serializers import ClassSerializer as CoreClassSerializer, CertificateTemplateSerializer

from django.contrib.auth import get_user_model

User = get_user_model()


class UserInDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = sorted(('cpf', 'email', 'institution', 'cities', 'city', 'occupation', 'courses',
                         'date_joined', 'last_login', 'full_name', 'topics_created',
                         'number_of_likes', 'comments_created',))

    comments_created = serializers.SerializerMethodField()
    full_name = serializers.SerializerMethodField()
    topics_created = serializers.SerializerMethodField()
    number_of_likes = serializers.SerializerMethodField()
    courses = serializers.SerializerMethodField()
    cities = serializers.SerializerMethodField()

    def get_cities(self, obj):
        contracts = [contract for group in obj.groups.all() for contract in group.contract.all()]
        group_names = [group.name.lower() for group in obj.groups.all()]
        city_names = [unity for c in contracts for unity in c.unities_normalized]
        return " - ".join(set(city_names) & set(group_names)).capitalize()

    def get_comments_created(self, obj):
        return obj.comment_author.count()

    def get_courses(self, obj):
        needed_stuff = [
            {'percent_progress': x.percent_progress(),
             'course_finished':  x.can_emmit_receipt(),
             'course_name': x.course.name,
             'has_certificate': x.certificate.type == 'certificate',
             'class_name': x.get_current_class().name
            } for x in obj.coursestudent_set.all()]
        return needed_stuff

    def get_full_name(self, obj):
        return obj.get_full_name()

    def get_topics_created(self, obj):
        return obj.topic_author.count()

    def get_number_of_likes(self, obj):
        return obj.topiclike_set.count() + obj.commentlike_set.count()


class UsersByClassSerializer(serializers.Serializer):
    # class Meta:
        # model = CourseStudent

        # fields = sorted(('cpf', 'email', 'full_name', 'last_login', 'has_certificate'))

    cpf = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    full_name = serializers.SerializerMethodField()
    last_login = serializers.SerializerMethodField()
    has_certificate = serializers.SerializerMethodField()
    percent_progress_by_lesson = serializers.SerializerMethodField()
    percent_progress = serializers.SerializerMethodField()
    course_finished = serializers.SerializerMethodField()
    class_name = serializers.SerializerMethodField()
    cities = serializers.SerializerMethodField()
    city = serializers.SerializerMethodField()
    occupation = serializers.SerializerMethodField()
    institution = serializers.SerializerMethodField()

    def get_city(self, obj):
        return obj.user.city

    def get_occupation(self, obj):
        return obj.user.occupation

    def get_institution(self, obj):
        return obj.user.institution

    def get_cpf(self, obj):
        return obj.user.cpf

    def get_cities(self, obj):
        obj = obj.user
        contracts = [contract for group in obj.groups.all() for contract in group.contract.all()]
        group_names = [group.name.lower() for group in obj.groups.all()]
        city_names = [unity for c in contracts for unity in c.unities_normalized]
        return " - ".join(set(city_names) & set(group_names)).capitalize()

    def get_email(self, obj):
        return obj.user.email

    def get_full_name(self, obj):
        return obj.user.get_full_name()

    def get_last_login(self, obj):
        return obj.user.last_login

    def get_has_certificate(self, obj):
        return obj.certificate.type == 'certificate'

    def get_percent_progress_by_lesson(self, obj):
        return obj.percent_progress_by_lesson()

    def get_percent_progress(self, obj):
        return obj.percent_progress()

    def get_course_finished(self, obj):
        return obj.can_emmit_receipt()

    def get_class_name(self, obj):
        return obj.get_current_class().name
