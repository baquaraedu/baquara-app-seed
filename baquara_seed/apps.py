from django.apps import AppConfig


class baquara_seedConfig(AppConfig):
    name = 'baquara_seed'
    verbose_name = 'baquara_seed'
