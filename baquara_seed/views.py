# -*- coding: utf-8 -*-
import json

from braces.views import LoginRequiredMixin
from django.conf import settings
from django.http import HttpResponse, Http404
from django.utils.decorators import method_decorator
from django.utils.text import slugify
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, DetailView
from django.core.exceptions import PermissionDenied

from rest_framework import viewsets, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny
from rest_framework.viewsets import ModelViewSet

from core.models import Course, CourseStudent, Class, CertificateTemplate, CourseCertification, StudentProgress
from core import views as core_views
from core.permissions import IsProfessorCoordinatorOrAdminPermissionOrReadOnly
from core.serializers import CertificateTemplateImageSerializer, CourseCertificationSerializer

from accounts.models import TimtecUser, Group
from accounts.views import GroupViewSet, GroupAdminViewSet

from baquara_seed.serializers import (UserInDetailSerializer,
    UsersByClassSerializer, ContractSerializer, ContractGroupSerializer)
from baquara_seed.models import Contract, CertificateData
from baquara_seed.serializers import ContractGroupAdminSerializer, ContractClassSerializer, \
    CertificateDataSerializer, CertificateImageDataSerializer, SimpleContractSerializer

from discussion.models import Comment, CommentLike, Topic, TopicLike
from rest_pandas import PandasViewSet
from rest_pandas.renderers import PandasCSVRenderer, PandasJSONRenderer
import pandas as pd

from administration.views import AdminMixin
from serializers import ClassSerializer, CourseGroupSerializer
from activities.models import Activity, Answer
from django.db.models import Count


class SummaryViewSet(viewsets.ViewSet):

    permission_classes = [IsAuthenticated]

    def list_new(self, request):

        courses = Course.objects.all()
        stats = {}
        activities = {}
        unit_set = {}
        all_answers = {}
        all_progress = {}
        classes = Class.objects.filter(course=courses)
        for course in courses:
            stats[course.slug] = {
                'name': course.name,
                'user_count': course.coursestudent_set.count(),
                'user_finished_course_count': 0,
                'classes' : []
            }
            unit_set[course.slug] = course.unit_set.count()
            le_activities = Activity.objects \
                .filter(unit__lesson__in=course.lessons
                        .filter(status='published'),
                        type="discussion")
            all_answers[course.slug] = {}
            all_progress[course.slug] = {}
            activities[course.slug] = le_activities.count()

            aux = Answer.objects \
                .filter(activity__in=le_activities)\
                .values('user')\
                .order_by('user')\
                .annotate(Count('user'))
            for ans in aux:
                all_answers[course.slug][ans['user']] = ans['user__count']

            aux = StudentProgress.objects\
                .exclude(complete=None) \
                .filter(unit__lesson__course=course)\
                .values('user')\
                .order_by('user').\
                annotate(Count('user'))

            for progress in aux:
                all_progress[course.slug][progress['user']] = progress['user__count']



        def __plpc_course_finished(activities, answers, units_len,
                                  units_done_len, min_percent_to_complete):
            return __percent_progress(units_len, units_done_len) > min_percent_to_complete and \
                   activities == answers

        def __percent_progress(units_len, units_done_len):
            if units_len <= 0:
                return 0
            return int(100.0 * units_done_len / units_len)

        def __can_emmit_receipt(cclass, certificate, answers, user):
            course_finished = __plpc_course_finished(activities[cclass.course.slug],
                                                    answers,
                                                    unit_set[cclass.course.slug],
                                                    all_progress[cclass.course.slug].get(user, 0),
                                                    cclass.course.min_percent_to_complete)
            if not cclass.user_can_certificate and not course_finished:
                return False
            if cclass.user_can_certificate_even_without_progress and certificate:
                return True
            return course_finished

        for cclass in classes:
            certified = cclass.get_students.filter(certificate__type='certificate')
            not_certified = cclass.get_students.exclude(certificate__type='certificate')
            cclass_stats = {
                'name': cclass.name,
                'user_count': cclass.get_students.count(),
                'certificate_count': certified.count(),
                'user_finished': 0
            }

            for cs in certified:
                answers = all_answers[cclass.course.slug].get(cs.user.id, 0)
                if __can_emmit_receipt(cclass, True, answers, cs.user.id):
                    stats[cclass.course.slug]['user_finished_course_count'] += 1
                    cclass_stats['user_finished'] += 1

            for cs in not_certified:
                answers = all_answers[cclass.course.slug].get(cs.user.id, 0)
                if __can_emmit_receipt(cclass, False, answers, cs.user.id):
                    stats[cclass.course.slug]['user_finished_course_count'] += 1
                    cclass_stats['user_finished'] += 1

            stats[cclass.course.slug]['classes'].append(cclass_stats)

        stats = list(stats.values())

        response = Response({
            'user_count': TimtecUser.objects.count(),
            'total_number_of_topics': Topic.objects.count(),
            'total_number_of_comments': Comment.objects.count(),
            'total_number_of_likes': TopicLike.objects.count() + CommentLike.objects.count(),
            'statistics_per_course': stats})

        return response

    def list(self, request):
        return self.list_new(request)

    def list_old(self, request):
        courses = Course.objects.all()
        stats = []
        for course in courses:
            course_stats = {
                'name': course.name,
                'user_count': course.coursestudent_set.count(),
                'user_finished_course_count': 0
            }
            classes = Class.objects.filter(course=course)
            classes_stats = []
            for cclass in classes:
                cclass_stats = {
                    'name': cclass.name,
                    'user_count': cclass.get_students.count(),
                    'certificate_count': 0,
                    'user_finished': 0
                }

                course_students = cclass.get_students.all()
                certified = course_students.filter(certificate__type='certificate')
                cclass_stats['certificate_count'] += certified.count()

                for cs in course_students:
                    # if cs.course_finished:
                    #    cclass_stats['user_finished'] += 1
                    if cs.can_emmit_receipt():
                        course_stats['user_finished_course_count'] += 1
                        cclass_stats['user_finished'] += 1

                classes_stats.append(cclass_stats)

            course_stats['classes'] = classes_stats
            stats.append(course_stats)

        response = Response({
            'user_count': TimtecUser.objects.count(),
            'total_number_of_topics': Topic.objects.count(),
            'total_number_of_comments': Comment.objects.count(),
            'total_number_of_likes': TopicLike.objects.count() + CommentLike.objects.count(),
            'statistics_per_course': stats})
        return response


class UsersByGroupViewSet(PandasViewSet):

    permission_classes = [IsAuthenticated]
    renderer_classes = [PandasCSVRenderer, PandasJSONRenderer]
    queryset = TimtecUser.objects.all()

    def list(self, request, format=None):
        groups = request.query_params.get('group', None)
        if groups is not None:
            serializer = UserInDetailSerializer(self.queryset.filter(groups__name__in=groups.split(',')), many=True)
        else:
            serializer = UserInDetailSerializer(self.queryset, many=True)
        # in order to get the data in the wanted column form, I'll need to make some transformations
        return Response(self.transform_data(serializer.data))

    def transform_data(self, data):
        response = []
        for user in data:
            for course in user.get('courses'):
                user.update({
                    u'{} - progresso'.format(course['course_name']): course['percent_progress'],
                    u'{} - nome da turma'.format(course['course_name']): course['class_name'],
                    u'{} - concluiu'.format(course['course_name']): course['course_finished'],
                    u'{} - possui certificado'.format(course['course_name']): course['has_certificate']
                })
            user.pop('courses', None)
            response.append(user)
        return pd.DataFrame.from_dict(response)


class UsersByClassViewSet(PandasViewSet):

    permission_classes = [IsAuthenticated]
    queryset = CourseStudent.objects.all()

    def list(self, request, format=None):
        try:
            ids = request.query_params.get('id', None).split(',')
            ids = [int(i) for i in ids]
        except AttributeError:
            ids = []

        classes = Class.objects.all().filter(pk__in=ids)
        students = [cls.students.all() for cls in classes]
        students = [s for cls in students for s in cls]
        courses = [cls['course_id'] for cls in classes.values()]

        queryset = self.queryset
        if len(ids) > 0:
            queryset = self.queryset \
                .filter(user__in=students, course__id__in=courses)

        serializer = UsersByClassSerializer(queryset, many=True)

        return Response(pd.DataFrame
                        .from_dict(self.transform_data(serializer.data))
                        .set_index('cpf'))

    def transform_data(self, data):
        for coursestudent in data:
            for lesson in coursestudent.get(u'percent_progress_by_lesson', None):
                coursestudent.update({
                    u'Lição {} - Progresso'.format(lesson['name']): lesson['progress']
                })
                for activity in lesson.get(u'activities', None):
                    coursestudent.update({
                        u'Lição {} - Atividade {} realizada'.format(lesson['name'], activity['name']): activity['done']
                    })
            coursestudent.pop('percent_progress_by_lesson', None)
        return data


@api_view(['POST'])
@permission_classes((IsAdminUser,))
def contract_uploader_view(request):
    csv_file = request.FILES.get('file', None)
    contract_id = request.data.get('contract_id', None)
    data = {
        'errors' : {
            'user_exists' : [],
            'class_not_found' : [],
        },
        'stats'  : {
            'inserted' : 0,
            'num_errors': 0,
            'new_groups':0,
        }
    }
    if not contract_id:
        data['errors']['form'] = "Contract must be specified"
        return Response(data, status.HTTP_400_BAD_REQUEST)

    contract = Contract.objects.get(pk=contract_id)
    if csv_file:
        import unicodecsv as csv
        transactions = {
            'create' : [],
            'group_users' : {},
            'class_users' : {},
            'contract' : {
                'group' : [],
                'class' : [],
                'unities' : []
            }
        }
        try:
            cf = csv_file.read().splitlines()
            csv_reader = csv.DictReader(cf)
            csv_reader = [c for c in csv_reader]

            groups = set([c for u in csv_reader for c in u['Grupos']
                         .split(';')])
            for group in groups:
                if not Group.objects.filter(name=group).exists():
                    g = Group(name=group)
                    transactions['create'].append(g)
                    data['stats']['new_groups']+=1

            for user in csv_reader:
                transactions['contract']['unities'] += user[u'Município']\
                    .split(';')
                if not TimtecUser.objects.filter(email=user['Email']).exists():
                    username = slugify((user['Nome'] + user['Sobrenome'])[0:30])
                    tries = 0
                    while TimtecUser.objects.filter(username=username)\
                        .exists() and tries < 10:
                        tries+=1
                        username = username[0:-1] + str(tries)

                    u = TimtecUser(email=user['Email'], cpf=user['CPF'],
                                   username=username,
                                   first_name=user['Nome'][0:30],
                                   last_name=user['Sobrenome'][0:30])
                    transactions['create'].append(u)
                    data['stats']['inserted']+=1
                else:
                    data['errors']['user_exists'].append(user['Nome'] +
                                                         user['Sobrenome'])
                    data['stats']['num_errors']+=1
                    u = None

                groups = user['Grupos'].split(';')
                for group in groups:
                    if not transactions['group_users'].has_key(group):
                        transactions['group_users'][group] = []

                    transactions['contract']['group'].append(group)
                    if u:
                        transactions['group_users'][group].append(u)

                classes = user['Turmas'].split(';')
                for class_course in classes:
                    (cclass, course) = class_course.split(' @ ')
                    if Class.objects.filter(name=cclass, course__slug=course)\
                        .exists():
                        if not transactions['class_users']\
                            .has_key(class_course):
                            transactions['class_users'][class_course] = []
                        if u:
                            transactions['class_users'][class_course].append(u)
                        transactions['contract']['class']\
                            .append(Class.objects
                                    .get(name=cclass, course__slug=course))
                    else:
                        data['errors']['class_not_found'].append(class_course)
                        data['stats']['num_errors']+=1

            if data['stats']['num_errors'] == 0:
                from django.db import transaction
                with transaction.atomic():
                    # create entities
                    for transaction in transactions['create']:
                        transaction.save()

                    # add to groups
                    for group_transaction in transactions['group_users']:
                        group = Group.objects.get(name=group_transaction)
                        for u in transactions['group_users'][group_transaction]:
                            group.user_set.add(u)
                        group.save()

                    # add to classes
                    for class_transaction in transactions['class_users']:
                        (cclass, course) = class_transaction.split(' @ ')
                        cclass = Class.objects\
                            .get(name=cclass, course__slug=course)
                        for u in transactions['class_users'][class_transaction]:
                            cclass.students.add(u)
                            cclass.save()
                            if not CourseStudent.objects\
                                .filter(user=u, course=cclass.course).exists():
                                cs = CourseStudent(user=u, course=cclass.course)
                                cs.save()

                    # add groups and classes to contract
                    groups = Group.objects\
                        .filter(name__in=transactions['contract']['group'])
                    for group in groups:
                        contract.groups.add(group)

                    # add classes to contract
                    classes = transactions['contract']['class']
                    for cclass in classes:
                        contract.classes.add(cclass)

                    unities = contract.unities + \
                              list(set(transactions['contract']['unities']))
                    contract.unities = unities
                    contract.save()
                    serializer = ContractSerializer(instance=contract)
                    data['instance'] = serializer.data
            else:
                serializer = ContractSerializer(instance=contract)
                data['instance'] = serializer.data
                return Response(data, status.HTTP_400_BAD_REQUEST)
        except KeyError:
            data['errors']['form'] = "Not valid CSV file"
            return Response(data, status.HTTP_400_BAD_REQUEST)
    else:
        data['errors']['form'] = "Not valid CSV file"
        return Response(data, status.HTTP_400_BAD_REQUEST)

    return Response(data, status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAdminUser,))
def contract_remove_users_view(request):
    CONTRACT_ARCHIVE_CLASS_PREFIX = "ARQUIVO_"
    contract_id = request.data.get('contract', None)
    if not contract_id:
        return Response({"error" : "Contrato não informado"},
                        status=status.HTTP_400_BAD_REQUEST)

    users = request.data.get('users', None)
    if users:
        users = users.split('\n')
    else:
        return Response({"error" : "Erro ao receber lista de usuários"},
                        status=status.HTTP_400_BAD_REQUEST)

    contract = Contract.objects.get(id=contract_id)
    contract_archive_group = u'Espaço Aberto %s' % (contract.name,)
    if not Group.objects.filter(name=contract_archive_group).exists():
        contract_archive_group = Group(name=contract_archive_group)
        contract_archive_group.save()
    else:
        contract_archive_group = Group.objects.get(name=contract_archive_group)

    groups_add = [Group.objects.get(name="Espaço Aberto"),
                  Group.objects.get(name="students"), contract_archive_group]

    errors = {
        'num_errors' : 0,
        'email_with_error' : [],
    }
    users_to_remove = []
    for u in users:
        if TimtecUser.objects.filter(email=u).exists():
            user = TimtecUser.objects.get(email=u)
            users_to_remove.append(user)
        else:
            errors['num_errors']+=1
            errors['email_with_error'].append(u)

    if errors['num_errors'] == 0:
        groups_remove = contract.groups.values_list('id', flat=True)
        contract_classes = contract.classes.exclude(name__contains="ARQUIVO_")
        archive_classes = {}
        for c in contract_classes:
            archive_class_name = "%s%s@%s" % \
                                 (
                                     CONTRACT_ARCHIVE_CLASS_PREFIX,
                                     c.course.name,
                                     contract.name
                                 )
            if not Class.objects \
                .filter(name=archive_class_name,
                        course=c.course,
                        contract__id=contract_id).exists():
                archive_class = Class(name=archive_class_name,
                                      course=c.course,)
                archive_class.save()
                archive_class.contract.add(contract_id,)
            else:
                archive_class = Class.objects.get(name=archive_class_name,
                                                     course=c.course,
                                                     contract__id=contract_id)
            archive_classes[c.name + c.course.name] = archive_class

        from django.db import transaction
        with transaction.atomic():
            for u in users_to_remove:
                for g in groups_remove:
                    u.groups.remove(g)

                for g in groups_add:
                    u.groups.add(g)

                for c in u.classes.all().exclude(name__contains="ARQUIVO_"):
                    if c.contract.first().id == contract.id:
                        c.remove_students(u)
                        archive_classes[c.name + c.course.name].add_students(u)

        return Response({}, status=status.HTTP_200_OK)
    else:
        return Response(errors, status=status.HTTP_400_BAD_REQUEST)


class CourseGroupViewSet(viewsets.ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseGroupSerializer
    permission_classes = (IsAuthenticated,)

    def update(self, request, *args, **kwargs):
        groups = request.data.get('groups', None)
        course_slug = request.data.get('slug', None)
        instance = Course.objects.get(slug=course_slug)
        if groups and type(groups[0]) == int:
            curr_groups = set(list(instance.groups.all().values_list('id', flat=True)))
            remove_groups = curr_groups - set(groups)
            add_groups = set(groups) - curr_groups

            for g in add_groups:
                instance.groups.add(g)

            for g in remove_groups:
                instance.groups.remove(g)

            instance.save()
            serializer = CourseGroupSerializer(instance)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response({'error' : 'no group set'},
                            status=status.HTTP_400_BAD_REQUEST)
