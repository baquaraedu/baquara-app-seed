{% load i18n %}

Você está recebendo este e-mail porque você ou alguém solicitou uma redefinição de senha para sua conta no ambiente virtual de aprendizagem da Formação Continuada Territorial.

Você pode ignorar esta mensagem se não solicitou esta redefinição de senha. 

Para redefinir sua senha, clique no link abaixo:

{{password_reset_url}}

Este é um e-mail automático. Por favor, não responda. Caso queira falar com nossa equipe, entre em contato através do formulário de contato disponível em https://formacao.educacao.ba.gov.br/

Agradecemos sua participação na Formação Continuada Territorial.