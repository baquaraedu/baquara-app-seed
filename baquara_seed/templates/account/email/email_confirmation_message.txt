Estamos felizes por poder ajudá-lo(a) nesse trajeto de formação e aprendizado.

Para garantir que você tenha acesso a todos os recursos e cursos que temos para oferecer, clique no link abaixo para confirmar o seu endereço de e-mail.

{{ activate_url }}

Este é um e-mail automático. Por favor, não responda. Caso queira falar com nossa equipe, entre em contato através do formulário de contato disponível em https://formacao.educacao.ba.gov.br/

Agradecemos sua participação na Formação Continuada Territorial.