(function(angular){
    'use strict';
    var app = angular.module('header.services', ['ngResource']);

    app.factory('UnreadNotification', ['$resource', function($resource){
        return $resource(BASE_API_URL + '/unread-notification/:id',
            {'id' : '@id'},
            {'update': {'method': 'PUT', 'ignoreLoadingBar': true} });
    }]);

    app.factory('AnswerNotification', ['$resource', function($resource){
        return $resource(BASE_API_URL + '/answer-notification/:id',
            {'id' : '@topic'},
            {'update': {'method': 'PUT', 'ignoreLoadingBar': true} });
    }]);

})(window.angular);
